<?php

class UserController extends App_Controller 
{
    public function index()
	{		
	       $usuarios = $this->UserModel->find([]);
		   $this->setVars(compact('usuarios')); 
		   $this->view = 'user/index';
	}
    public function save()
	{
		
		$date = new DateTime($this->data['dataNasc']);
        $this->data['dataNasc'] = $date->format('Y-m-d H:i:s');
		
		$this->data['data'] = date('Y-m-d');
        if ($this->UserModel->save($this->data)) {
		   $this->setAlert("Cadastro realizado com sucesso", 'index');	
		} else {
		   $this->setAlert("Erro ao salvar", 'index');	
		}
		
    }
	public function cadastro()
	{
		$this->view = 'user/cadastro';
    }
	
	public function remover($data)
	{   extract($data);
		$this->UserModel->delete($id);
		$this->setAlert("Registro deletdo", '../');

        $this->view = false;		
    }
	public function editar($data)
	{   extract($data);
		$usuario = $this->UserModel->find([
		  'id' => $id
		]);
		$usuario = reset($usuario);
        $this->setVars(compact('usuario'));
        $this->view = 'user/editar';		
    }
	public function editar_salvar($data)
	{  
	
		$date = new DateTime($this->data['dataNasc']);
        $this->data['dataNasc'] = $date->format('Y-m-d H:i:s');
		
		$id = $this->data['id'];
		unset($this->data['id']);
       
		if ($this->UserModel->update($this->data, ['filtro'=> ['id' =>$id ]])) {
		   $this->setAlert("Usuario atualizado com sucesso", 'index');	
		} else {
		   $this->setAlert("Erro ao salvar", 'index');	
		}		
    }

}