<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?echo $title; ?></title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
	<div class="container">
	      <div class="row">
		   <h2>Listagem de Usuarios</h2>
 
		  <table class="table">
			<thead>
			  <tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Data cadastro</th>
				<th>Data Nascimento</th>
			  </tr>
			</thead>
			<tbody>
		        <?php  foreach($usuarios as $usuario) :?>
		               <tr> 
					        <td><?= $usuario['nome']?></td>
                            <td><?= $usuario['email']?></td>
                            <td><?= $usuario['data']?></td>
                            <td><?= $usuario['dataNasc']?></td>
							<td> <a href="editar/?id=<?= $usuario['id']?>"> EDITAR </a></td>
							<td> <a href="remover/?id=<?= $usuario['id']?>"> DELETAR</a></td>
                       </tr>
				<?php  endforeach;?>
		    </tbody>
			  <a href="cadastro" class="btn btn-primary">Cadastrar</a>
		   </div>
     </div>
	</body>
</html>