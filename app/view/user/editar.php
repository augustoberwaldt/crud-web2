<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?echo $title; ?></title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
	<div class="container">
	      <div class="row">
			   <div class="col-md-6 offset-md-6">
				  <h3> Formulario de Modificação  de Usuario </h3>
			   </div>
		  </div> 
		  <div class="row">
			  <form class="form-horizontal" method="POST" action="../editar_salvar" >
			       <input type="hidden" name="id"  value="<?=$usuario['id'] ?>">
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Nome</label>
					<div class="col-sm-10">
					  <input type="text" name="nome"  value="<?=$usuario['nome'] ?>"class="form-control" id="inputNome" placeholder="Nome">
					</div>
				  </div>
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" name="email" value="<?=$usuario['email'] ?>" class="form-control" id="inputEmail" placeholder="Email">
					</div>
				  </div>
				  <div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-10">
					  <input type="password" name="senha" value="<?=$usuario['senha'] ?>" class="form-control" id="inputPassword3" placeholder="Password">
					</div>
				  </div>
				  <div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Data de Nascimento</label>
					<div class="col-sm-10">
					  <div class="input-group date" data-provide="datepicker">
						<input  value="<?=$usuario['dataNasc'] ?>" name="dataNasc" placeholder="Data de Nascimento" type="text" class="form-control datepicker">
					
					    </div>
					</div>
				  </div>
				 
				  <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					  <button type="submit" class="btn btn-default">Alterar</button>
					</div>
				  </div>
			   </form>
		  </div>
     </div>
	</body>
	<script>
	$('.datepicker').datepicker();
	</script>
</html>