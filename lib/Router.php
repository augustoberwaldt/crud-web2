<?php

class Router {

    
	public static function rota($url) 
	{
		$url = preg_replace('/^[\/]|[\/]$/',"",$url);
		
		if ($url == 'index' || $url == '/' ) {
			return [];
		}
		
		$url = preg_replace('/((&|\?)(.*)=[^&]*)/', "", $url);
		
	    return $url;     
		
	}

}