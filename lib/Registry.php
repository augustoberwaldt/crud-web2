<?php
class Registry {
	
	private $objects = array();
	public static function set ($key, $object) {
		$self =& ClassRegistry::getInstance();
		if (!isset($self->objects[$key])) {
			$self->objects[$key] =& $object;
			return true;
		}
		
		return false;
	}
	
	public static function  &get($key) {
		$self =& ClassRegistry::getInstance();
		$return = false;
		if (isset($self->objects[$key])) {
			$return =& $self->objects[$key];
		}
		
		return $return;
	}
}
	