﻿<?php 
require_once LIB.DS.'Router.php';
class Dispatcher {
    const  ACTION_PADRAO  = 'index';
	const  CONTROLLER_PADRAO  = 'index';
   
    public $controller;
    public  $action;	
	public function __construct() 
	{
		
		$this->getActionController();
 	    $this->controller = preg_replace('/((&|\?)(.*)=[^&]*)/', "", $this->controller );	
		$controller_class = ucwords($this->controller).'Controller';
		$model_class = ucwords($this->controller).'Model';
    	
		$file = CONTROLLER.DS.$controller_class . '.php';
		$model= MODEL.DS.$model_class.'.php';
	
		if (!file_exists($file)) {
			$this->pageNotFound();
		}
     		 
		require_once  $file;
	    if (!class_exists($controller_class, false)) {
			$this->pageNotFound();
		}                            		 
	    $object = new $controller_class();   
		if (!method_exists($object, $this->action)){
			$this->pageNotFound();
		}
	
		if (file_exists($model)){
     	   require_once $model;
		   if (class_exists($model_class)) {	
				$model_obj = new $model_class();	         
				$object->$model_class = $model_obj;
		   }
		}
		
		session_start();
       
        
		$data = array_merge($_GET, $_POST);
		$object->data = $data;
		call_user_func_array([$object, $this->action], ['get' => $_GET ,'post' =>$_POST]);
		
		if ($object->useView()) {
			$object->render();
		}
		unset($object);
	}
	private function pageNotFound()
	{
		header("HTTP/1.0 404 Not Found");
		Load::loadView('404');
		exit();
	}
    private function getActionController()
	{
		$request = $this->getbaseUrl();
		$request = preg_replace('/((&|\?)(.*)=[^&]*)/', "", $request);

		$params     = explode('/', $request);
	    $params  = array_filter($params);
	
		if (count($params) == 1) {
			$action     = self::ACTION_PADRAO;
		    $controller = $params[key($params)];
		} else {	
		   $action     = empty(end($params)) ? self::ACTION_PADRAO : end($params);
		   $controller = empty($params[count($params) - 1]) ? self::CONTROLLER_PADRAO : $params[count($params) - 1];
	       $action = preg_replace('/((&|\?)(.*)=[^&]*)/',"",$action);
		}	 
	    $this->controller = $controller;
		$this->action     =  $action;
		
		return [
		  'action'     => $action,
		  'controller' => $controller
	    ];
	}
    private  function getbaseUrl() 
	{
		$request    = str_ireplace($_SERVER['SERVER_NAME'], "", $_SERVER['REQUEST_URI']);
	    $dirRoot    = str_replace('\\','/',dirname(  dirname(__FILE__)));
	    $dirRoot    = str_replace($_SERVER['DOCUMENT_ROOT'], '', $dirRoot);
		$dirRoot    = $dirRoot . '/';

		$request    = str_ireplace([$dirRoot , '/CRUD/'],	"/",	$request);		
		$request    = str_ireplace('//',	"",	$request);		
	
		return  	$request;
	}
}