<?php

abstract class Model {

    private static $connection = NULL;
    private static $user;
    private static $pass;
    private static $dbname;
    private static $host;
	private static $driver;
    private static $dbHandle = NULL;
    protected $table = NULl;
    protected $pk = NULl;


    private static final function getConnection()
	{

        if (!self::$connection) {
             try {

				$host     = self::$host;
				$dbname   = self::$dbname;
				$password = self::$pass;
				$username = self::$user;
				$driver = self::$driver;
				self::$connection = new PDO($driver. ":dbname=$dbname;host=$host;user=$username; password=$password;",$username ,$password);
			 } catch(PDOException $e){

				printrx($e);
			 }
		}
    }
	

    public final function __construct()
	{
		$dados = include 'config/database.php';

		self::$host   = $dados['host'];
		self::$dbname = $dados['schema'];
		self::$user   = $dados['user'];
		self::$pass   = $dados['pass'];
		self::$driver = $dados['driver'];

        self::getConnection();
        if (is_null($this->table) &&  $this->table != 'generica') {
            die("Tabela nao setada");
		}
        if (is_null($this->pk)) {
            die("Primary Key não setada na propriedade pk");
		}
	}

    public final function query($sql)
	{
		$result = self::$connection->prepare($sql);
	    $result->execute();
		return  $result;
    }

    public function delete($id) {
        $sql = "DELETE FROM " . $this->table . " WHERE " . $this->pk . "='" . $id . "'";
        return $this->query($sql);
    }
	
	public function find($param)
	{
		$fields = '';
		if (isset($param['campos']) && is_array($param['campos']))
		{
		   $fields =  implode(',', $param['campos']);
		   if (empty($fields)) {
				$fields = '*';
		   }
		}else {
			$fields = '*';
		}
		$tabela = isset($param['tabela']) ? $filtro['tabela'] : $this->table;
		$where  = '';
		$sql = sprintf("SELECT %s FROM  %s ",$fields , $tabela);

		if (isset($param['filtro']) && is_array($param['filtro'])) {
            foreach ($param['filtro'] as $key => $value) {
         
			   if (is_numeric($key)) {
				 $where .= ' '.$value;
               }elseif($this->has_next($param['filtro']) ){
				 $where .=   ' AND ' . $key .' = '. "'".$value."'" ;   
			   } else {
                 $where .= $key .' = '."'".$value."'";
               }
            }
		} else {
		     if (!empty($param['filtro']) && is_string($param['filtro'])) {
				 $where  = $param['filtro'];
			 }

		}
		$joinSql = '';
        if (isset($param['join'])) {
        	$joinSql = $this->genJoin($param['join']); 
        } 
        if (!empty($joinSql)) {

        	$sql = $sql . $joinSql;
        }

	    $wheresql = 'WHERE  '. $where;
	    if (!empty($where)) {
	       	$sql = $sql . $wheresql;
	    }
        if (isset($param['ordem'])) {
            $sql = $sql . 'ORDER BY '. $param['ordem'];
        }            
       

        if (isset($param['limite'])) {
        	$sql .= ' LIMIT '. $param['limite']; 	
        }
        if (isset($param['linhas']) &&  isset($param['limite'])) {
        	$sql .= 'OFFSET '. $param['limite']; 	
        }


		if (isset($param['sql'])) {
			   return $sql;
		}
		
        $ret = $this->query($sql);
        $row = $ret->fetchAll(PDO::FETCH_ASSOC);
		return  $row ;

	}

	public function save(array $param, $filtro = [])
	{
		if (empty($param)) {
			return false;
		}
	    $tabela = isset($filtro['tabela']) ? $filtro['tabela'] : $this->table;
		$fields  = array_keys($param);
		$fields  = implode(',', $fields);
		$valuesArray  = array_values($param);
		$valuesArray = array_map(function($elemento){
			return "'" . $elemento . "'";
		} , $valuesArray);

		$values  = implode(',', $valuesArray);
		$sql = sprintf("INSERT INTO %s(%s) values(%s);" ,$tabela  ,$fields , $values);
		if (isset($filtro['sql'])) {
			return $sql;
		}

		$ret = $this->query($sql);

		if ($ret) {
			return true;
		}
		return false;
	}
	public function update(array $param, $filtro = [])
	{
		if (empty($param)) {
			return false;
		}
	    $tabela = isset($filtro['tabela']) ? $filtro['tabela'] : $this->table;
		$fields  = '';
		$count = 1;		
		foreach($param as $key => $value) {
		    $fields.=  $key . " = '" . $value . "'".($count != count($param) ? ' ,' : '');  	
		    $count++;
		}
		$where = 'WHERE ';
		foreach($filtro['filtro'] as $key => $value) {
		   $where.= $key . "= '". $value. "'"; 
		    $count++;
		}


		$valuesArray  = array_values($param);
		

		$values  = implode(',', $valuesArray);
		$sql = sprintf("UPDATE %s SET %s   %s;" ,$tabela  ,$fields , $where);
		if (isset($filtro['sql'])) {
			return $sql;
		}

		$ret = $this->query($sql);

		if ($ret) {
			return true;
		}
		return false;
	}

  private function has_next($array) {
    if (is_array($array)) {
        if (next($array) === false) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
 }

  /**
     * Generate SQL Query
     */
    private function genJoin($join)
    {
        $sqlJoin = '';
        if ($join) {
            if (is_array($join)) {
                foreach ($join as $key => $value) {
                    if (is_array($value)) {
                        // Permite forma abreviada
                        if (!isset($value['on']) && !isset($value['type']) && !isset($value['tabela']) && !isset($value['sql']) && $value) {
                            $value = array('on' => $value);
                        }

                        $joinType = (isset($value['type'])) ? strtoupper($value['type']) : 'LEFT';

                        $joinAlias = $key;
                        $joinTable = (isset($value['tabela'])) ? $value['tabela'] : $key;

                        if (isset($value['on'])) {
                            $joinOn = ' ON ';
                            if (is_array($value['on'])) {
                                foreach ($value['on'] as $vkey => $vvalue) {
                                    $joinOn .= is_int($vkey) ? "{$vvalue} AND " : "{$vkey} = {$vvalue} AND ";
                                }
                                $joinOn = substr($joinOn, 0, -5);
                            } elseif (strtolower(substr($value['on'], 0, 5)) == 'using') {
                                $joinOn = $value['on'];
                            } else {
                                $joinOn .= $value['on'];
                            }
                        } else {
                            $joinOn = "USING ($this->primary)";
                        }

                        if (!empty($value['sql'])) {
                            $joinTable = $value['sql'];
                        } else {
                            $joinTable = ($joinTable);
                        }

                        $sqlJoin .= "\n$joinType JOIN $joinTable AS $joinAlias $joinOn";
                    } elseif ($value !== false) {
                        $joinAlias = ucfirst($value);
                        $joinTable = $this->tablesDic(strtolower($value));
                        $sqlJoin .= "\nLEFT JOIN " . $joinTable . " AS " . $joinAlias . " USING ($this->primary)";
                    }
                }
            } else {
                if (strtolower(substr($join, 0, 4)) == 'left' || strtolower(substr($join, 0, 5)) == 'right' || strtolower(substr($join, 0, 5)) == 'inner') {
                    $sqlJoin = $join;
                } else {
                    $sqlJoin = "\nLEFT JOIN $join";
                }
            }
        }

        return $sqlJoin;
    }
}